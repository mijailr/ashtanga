ActiveAdmin.register Noti do

  permit_params :photo, :content, :title
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
    controller do
      def find_resource
        scoped_collection.friendly.find(params[:id])
      end
    end

    index do
      selectable_column
      column :id
      column :title
      column :created_at
      actions
    end

    form :html => { :enctype => "multipart/form-data" } do |f|
      f.inputs "Noti" do
        f.input :title
        f.input :content
        f.input :photo, require: false, as: :file
      end
      f.actions
    end


end
