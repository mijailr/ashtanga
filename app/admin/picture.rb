ActiveAdmin.register Picture do
  permit_params :photo, :position, :title

  index do
    selectable_column
    column :id

    column :position
    column :title
    column :created_at
    actions
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Galleria Photo" do
      f.inputs :position
      f.inputs :title
      f.input :photo, require: false, as: :file
    end
    f.actions
  end
end
