ActiveAdmin.register Orari do
  permit_params :day, :from, :to, :location_id
  form do |f|
    f.inputs 'Orari' do
      f.input :location_id, as: :select, collection: Location.pluck(:name, :id)
      f.input :day, as: :select, collection: Orari::WEEK.zip((0..6).to_a)
      f.input :from, as: :time_select, ignore_date: true
      f.input :to, as: :time_select, ignore_date: true
    end
    f.actions
  end
  index do
    selectable_column
    column :location
    column :day do |orari|
      Orari::WEEK[orari.day].capitalize
    end
    column :from do |orari|
      orari.from.strftime("%H:%M")
    end
    column :to do |orari|
      orari.to.strftime("%H:%M")
    end
    actions
  end
end
