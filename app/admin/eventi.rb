ActiveAdmin.register Eventi do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :photo, :date, :location, :description, :title
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id])
    end
  end

  index do
    selectable_column
    column :id
    column :title
    column :location
    column :created_at
    actions
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Eventi" do
      f.input :title
      f.input :description
      f.input :location
      f.input :date
      f.input :photo, require: false, as: :file
    end
    f.actions
  end
end
