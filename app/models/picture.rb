class Picture < ActiveRecord::Base
  has_attached_file :photo, styles: {
    medium: "900x675>",
    thumb: "390x228#",
  },
  url: "/assets/galery/:id/:style/:id.:extension",
  path: "#{ENV['OPENSHIFT_DATA_DIR']}public/assets/galery/:id/:style/:id.:extension"
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
end
