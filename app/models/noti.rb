class Noti < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  has_attached_file :photo, styles: {
    medium: "900x675>",
    thumb: "390x210#",
    index: "523x261#"
  },
  url: "/assets/noti/:id/:style/:id.:extension",
  path: "#{ENV['OPENSHIFT_DATA_DIR']}public/assets/noti/:id/:style/:id.:extension"
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
end
