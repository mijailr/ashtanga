class Orari < ActiveRecord::Base
  WEEK = [
    "lunedì",
    "martedì",
    "mercoledì",
    "giovedì",
    "venerdì",
    "sabato",
    "domenica"
  ]
  belongs_to :location
end
