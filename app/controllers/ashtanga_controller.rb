class AshtangaController < ApplicationController
  def index
    @eventi = Eventi.last
    @noti = Noti.last
  end
  def vinyasa
    @custom_title = "Il sistema dei Vinyasa"
  end
  def tradizione
    @custom_title = "La Tradizione"
  end
  def bed
    @custom_title = "Bed & Breakfast"
  end
  def contacti
    @custom_title = "Contattaci"
  end
end
