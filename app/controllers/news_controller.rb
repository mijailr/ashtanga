class NewsController < ApplicationController
  def index
    @custom_title = "Yoga News"
    @notis = Noti.page(params[:page]).per(5).order('created_at DESC')
  end

  def show
    @noti = Noti.friendly.find(params[:id])
    @custom_title = "#{@noti.title}"
  end
end
