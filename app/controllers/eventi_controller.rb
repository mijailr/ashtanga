class EventiController < ApplicationController
  def index
    @custom_title = "Yoga Eventis"
    @eventis = Eventi.page(params[:page]).per(5).order('created_at DESC')
  end

  def show
    @eventi = Eventi.friendly.find(params[:id])
    @custom_title = "#{@eventi.title}"
  end
end
