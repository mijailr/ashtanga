class GalleryController < ApplicationController
  def index
    @custom_title = "Galleria"
    @pictures = Picture.order("position ASC").all
  end
end
