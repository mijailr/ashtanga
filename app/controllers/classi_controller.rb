class ClassiController < ApplicationController
  def index
    @custom_title = "Classi e Orari"
    @locations= Location.order(created_at: :desc)
  end

  def show
    @location = Location.find(params[:id])
    @custom_title = "#{@location.name}"
  end
end
