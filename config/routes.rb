Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root 'ashtanga#index'
  get '/eventi' => 'eventi#index', as: :eventis
  get '/eventi/:id' => 'eventi#show', as: :eventi
  get '/galleria' => 'gallery#index', as: :galleria
  get '/tradizione' => 'ashtanga#tradizione', as: :tradizione
  get '/vinyasa' => 'ashtanga#vinyasa', as: :vinyasa
  get '/istruttore' => 'ashtanga#istruttore', as: :istruttore
  get '/classi' => 'classi#index', as: :classi
  get '/bed' => 'ashtanga#bed', as: :bed
  get '/news' => 'news#index', as: :news
  get '/news/:id' => 'news#show', as: :noti
  get '/contattaci' => 'ashtanga#contacti', as: :contacti
  # get '/news/:id' => 'noti#show', as: :news_show


end
