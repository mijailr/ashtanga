---
it:
  activerecord:
    errors:
      messages:
        record_invalid: 'Validazione fallita: %{errors}'
        restrict_dependent_destroy:
          has_one: Il record non può essere cancellato perchè esiste un %{record} dipendente
          has_many: Il record non può essere cancellato perchè esistono %{record} dipendenti
  date:
    abbr_day_names:
    - dom
    - lun
    - mar
    - mer
    - gio
    - ven
    - sab
    abbr_month_names:
    -
    - gen
    - feb
    - mar
    - apr
    - mag
    - giu
    - lug
    - ago
    - set
    - ott
    - nov
    - dic
    day_names:
    - domenica
    - lunedì
    - martedì
    - mercoledì
    - giovedì
    - venerdì
    - sabato
    formats:
      default: "%d/%m/%Y"
      long: "%d %B %Y"
      short: "%d %b"
    month_names:
    -
    - gennaio
    - febbraio
    - marzo
    - aprile
    - maggio
    - giugno
    - luglio
    - agosto
    - settembre
    - ottobre
    - novembre
    - dicembre
    order:
    - :day
    - :month
    - :year
  datetime:
    distance_in_words:
      about_x_hours:
        one: circa un'ora
        other: circa %{count} ore
      about_x_months:
        one: circa un mese
        other: circa %{count} mesi
      about_x_years:
        one: circa un anno
        other: circa %{count} anni
      almost_x_years:
        one: circa 1 anno
        other: circa %{count} anni
      half_a_minute: mezzo minuto
      less_than_x_minutes:
        one: meno di un minuto
        other: meno di %{count} minuti
      less_than_x_seconds:
        one: meno di un secondo
        other: meno di %{count} secondi
      over_x_years:
        one: oltre un anno
        other: oltre %{count} anni
      x_days:
        one: 1 giorno
        other: "%{count} giorni"
      x_minutes:
        one: 1 minuto
        other: "%{count} minuti"
      x_months:
        one: 1 mese
        other: "%{count} mesi"
      x_seconds:
        one: 1 secondo
        other: "%{count} secondi"
    prompts:
      day: Giorno
      hour: Ora
      minute: Minuto
      month: Mese
      second: Secondi
      year: Anno
  errors:
    format: "%{attribute} %{message}"
    messages:
      accepted: deve essere accettata
      blank: non può essere lasciato in bianco
      present: deve essere lasciato in bianco
      confirmation: non coincide con %{attribute}
      empty: non può essere vuoto
      equal_to: deve essere uguale a %{count}
      even: deve essere pari
      exclusion: è riservato
      greater_than: deve essere maggiore di %{count}
      greater_than_or_equal_to: deve essere maggiore o uguale a %{count}
      inclusion: non è compreso tra le opzioni disponibili
      invalid: non è valido
      less_than: deve essere minore di %{count}
      less_than_or_equal_to: deve essere minore o uguale a %{count}
      not_a_number: non è un numero
      not_an_integer: non è un numero intero
      odd: deve essere dispari
      required: deve esistere
      taken: è già presente
      too_long:
        one: è troppo lungo (il massimo è 1 carattere)
        other: è troppo lungo (il massimo è %{count} caratteri)
      too_short:
        one: è troppo corto (il minimo è 1 carattere)
        other: è troppo corto (il minimo è %{count} caratteri)
      wrong_length:
        one: è della lunghezza sbagliata (deve essere di 1 carattere)
        other: è della lunghezza sbagliata (deve essere di %{count} caratteri)
      other_than: devono essere di numero diverso da %{count}
    template:
      body: 'Ricontrolla i seguenti campi:'
      header:
        one: 'Non posso salvare questo %{model}: 1 errore'
        other: 'Non posso salvare questo %{model}: %{count} errori.'
  helpers:
    select:
      prompt: Seleziona...
    submit:
      create: Crea %{model}
      submit: Invia %{model}
      update: Aggiorna %{model}
  number:
    currency:
      format:
        delimiter: "."
        format: "%n %u"
        precision: 2
        separator: ","
        significant: false
        strip_insignificant_zeros: false
        unit: "€"
    format:
      delimiter: "."
      precision: 2
      separator: ","
      significant: false
      strip_insignificant_zeros: false
    human:
      decimal_units:
        format: "%n %u"
        units:
          billion: Miliardi
          million: Milioni
          quadrillion: Biliardi
          thousand: Mila
          trillion: Bilioni
          unit: ''
      format:
        delimiter: ''
        precision: 3
        significant: true
        strip_insignificant_zeros: true
      storage_units:
        format: "%n %u"
        units:
          byte:
            one: Byte
            other: Byte
          gb: GB
          kb: KB
          mb: MB
          tb: TB
    percentage:
      format:
        delimiter: ''
        format: "%n%"
    precision:
      format:
        delimiter: ''
  support:
    array:
      last_word_connector: " e "
      two_words_connector: " e "
      words_connector: ", "
  time:
    am: am
    formats:
      default: "%d/%m/%Y"
      long: "%d/%m/%Y"
      short: "%d/%m/%Y"
    pm: pm
  devise:
    confirmations:
      confirmed: "Il tuo account è stato correttamente confermato."
      send_instructions: "Entro qualche minuto riceverai un messaggio email con le istruzioni per confermare il tuo account."
      send_paranoid_instructions: "Se il tuo indirizzo email esiste nel nostro database, entro qualche minuto riceverai un messaggio email con le istruzioni per confermare il tuo account."
    failure:
      already_authenticated: "Hai già effettuato l'accesso."
      inactive: "Il tuo account non è ancora stato attivato."
      invalid: "%{authentication_keys} o password non validi."
      locked: "Il tuo account è bloccato."
      last_attempt: "Hai un altro tentativo prima che il tuo account venga bloccato."
      not_found_in_database: "%{authentication_keys} o password non validi."
      timeout: "La tua sessione è scaduta, accedi nuovamente per continuare."
      unauthenticated: "Devi accedere o registrarti per continuare."
      unconfirmed: "Devi confermare il tuo indirizzo email per continuare."
    mailer:
      confirmation_instructions:
        subject: "Istruzioni per la conferma"
      reset_password_instructions:
        subject: "Istruzioni per reimpostare la password"
      unlock_instructions:
        subject: "Istruzioni per sbloccare l'account"
    omniauth_callbacks:
      failure: 'Non è stato possibile autenticarti come %{kind} perché "%{reason}".'
      success: "Autenticato con successo dall'account %{kind}."
    passwords:
      no_token: "Non è possibile accedere a questa pagina se non provieni da una e-mail di ripristino della password. Se provieni da una e-mail di ripristino della password, assicurarti di utilizzare l'URL completo."
      send_instructions: "Entro qualche minuto riceverai un messaggio email con le istruzioni per reimpostare la tua password."
      send_paranoid_instructions: "Se il tuo indirizzo email esiste nel nostro database, entro qualche minuto riceverai un messaggio email con le istruzioni per ripristinare la password."
      updated: "La tua password è stata cambiata correttamente. Ora sei collegato."
      updated_not_active: "La tua password è stata cambiata correttamente."
    registrations:
      destroyed: "Arrivederci! Il tuo account è stato cancellato. Speriamo di rivederti presto."
      signed_up: "Benvenuto! Ti sei registrato correttamente."
      signed_up_but_inactive: "Ti sei registrato correttamente. Tuttavia non puoi effettuare l'accesso perché il tuo account non è stato ancora attivato."
      signed_up_but_locked: "Ti sei registrato correttamente. Tuttavia non puoi effettuare l'accesso perché il tuo account è bloccato."
      signed_up_but_unconfirmed: "Ti sei registrato correttamente. Un messaggio con il link per confermare il tuo account è stato inviato al tuo indirizzo email."
      update_needs_confirmation: "Il tuo account è stato aggiornato, tuttavia è necessario verificare il tuo nuovo indirizzo email. Entro qualche minuto riceverai un messaggio email con le istruzioni per confermare il tuo nuovo indirizzo email."
      updated: "Il tuo account è stato aggiornato."
    sessions:
      signed_in: "Accesso effettuato con successo."
      signed_out: "Sei uscito correttamente."
      already_signed_out: "Sei uscito correttamente."
    unlocks:
      send_instructions: "Entro qualche minuto riceverai un messaggio email con le istruzioni per sbloccare il tuo account."
      send_paranoid_instructions: "Se il tuo indirizzo email esiste nel nostro database, entro qualche minuto riceverai un messaggio email con le istruzioni per sbloccare il tuo account."
      unlocked: "Il tuo account è stato correttamente sbloccato. Accedi per continuare."
  errors:
    messages:
      already_confirmed: "è stato già confermato, prova ad effettuare un nuovo accesso"
      confirmation_period_expired: "deve essere confermato entro %{period}, si prega di richiederne uno nuovo"
      expired: "è scaduto, si prega di richiederne uno nuovo"
      not_found: "non trovato"
      not_locked: "non era bloccato"
      not_saved:
        one: "Un errore ha impedito di salvare questo %{resource}:"
        other: "%{count} errori hanno impedito di salvare questo %{resource}:"
  date:
    am: am
    formats:
      default: "%d/%m/%Y"
      long: "%d/%m/%Y"
      short: "%d/%m/%Y"
    pm: pm
  active_admin:
    dashboard: Dashboard
    dashboard_welcome:
      welcome: "Benvenuti in Active Admin. Questa è la pagina dashboard di default."
      call_to_action: "Per aggiungere sezioni alla dashboard controlla il file 'app/admin/dashboard.rb'"
    view: "Mostra"
    edit: "Modifica"
    delete: "Rimuovi"
    delete_confirmation: "Sei sicuro di volerlo rimuovere?"
    new_model: "Aggiungi %{model}"
    edit_model: "Modifica %{model}"
    delete_model: "Rimuovi %{model}"
    details: "Dettagli %{model}"
    cancel: "Annulla"
    empty: "Vuoto"
    previous: "Precedente"
    next: "Prossimo"
    download: "Scarica:"
    has_many_new: "Aggiungi nuovo/a %{model}"
    has_many_delete: "Rimuovi"
    has_many_remove: "Rimuovi"
    filters:
      buttons:
        filter: "Filtra"
        clear: "Rimuovi filtri"
      predicates:
        contains: "Contiene"
        equals: "Uguale a"
        starts_with: "Inizia con"
        ends_with: "Finisce con"
        greater_than: "Maggiore di"
        less_than: "Minore di"
    search_status:
      headline: "Situazione filtri:"
      current_scope: "Contesto selezionato:"
      current_filters: "Filtri attivi:"
      no_current_filters: "Nessuno"
    status_tag:
      "yes": "Sì"
      "no": "No"
    main_content: "Devi implemetare %{model}#main_content per mostrarne il contenuto."
    logout: "Esci"
    powered_by: "Powered by %{active_admin} %{version}"
    sidebars:
      filters: "Filtri"
      search_status: "Informazioni sulla ricerca"
    pagination:
      empty: "Nessun %{model} trovato"
      one: "Sto mostrando <b>1</b> %{model}"
      one_page: "Sto mostrando <b>%{n}</b> %{model}. Lista completa."
      multiple: "Sto mostrando %{model} <b>%{from}&nbsp;-&nbsp;%{to}</b> di <b>%{total}</b> in totale"
      multiple_without_total: "Sto mostrando %{model} <b>%{from}&nbsp;-&nbsp;%{to}</b>"
      entry:
        one: "voce"
        other: "voci"
    any: "Qualsiasi"
    blank_slate:
      content: "Non sono presenti %{resource_name}"
      link: "Crea nuovo/a"
    dropdown_actions:
      button_label: "Azioni"
    batch_actions:
      button_label: "Azioni multiple"
      default_confirmation: "Sei sicuro di che voler fare questo?"
      delete_confirmation: "Sei sicuro di volere cancellare %{plural_model}?"
      succesfully_destroyed:
        one: "Eliminato con successo 1 %{model}"
        other: "Eliminati con successo %{count} %{plural_model}"
      selection_toggle_explanation: "(Toggle Selection)"
      link: "Crea uno"
      action_label: "%{title} Selezionati"
      labels:
        destroy: "Elimina"
    comments:
      created_at: "Creato il"
      resource_type: "Tipo di risorsa"
      author_type: "Tipo di Autore"
      body: "Corpo"
      author: "Autore"
      title: "Commento"
      add: "Aggiungi Commento"
      delete: "Cancella Commento"
      delete_confirmation: "Sei sicuro di voler cancellare questo commento?"
      resource: "Risorsa"
      no_comments_yet: "Nessun commento."
      author_missing: "Anonimo"
      title_content: "Commenti (%{count})"
      errors:
        empty_text: "Il commento non può essere salvato, il testo è vuoto."
    devise:
      username:
        title: "Nome Utente"
      email:
        title: "Email"
      subdomain:
        title: "Sottodominio"
      password:
        title: "Password"
      sign_up:
        title: "Iscriviti"
        submit: "Iscriviti"
      login:
        title: "Entra"
        remember_me: "Ricordami"
        submit: "Entra"
      reset_password:
        title: "Dimenticato la password?"
        submit: "Reimposta la tua password"
      change_password:
        title: "Cambia la tua password"
        submit: "Cambia la mia password"
      unlock:
        title: "Invia di nuovo le istruzioni per sbloccare"
        submit: "Invia di nuovo le istruzioni per sbloccare"
      resend_confirmation_instructions:
        title: "Invia di nuovo le istruzioni per la conferma"
        submit: "Invia di nuovo le istruzioni per la conferma"
      links:
        sign_in: "Iscriviti"
        sign_in: "Entra"
        forgot_your_password: "Dimenticato la password?"
        sign_in_with_omniauth_provider: "Collegati a %{provider}"
        resend_unlock_instructions: "Invia di nuovo le istruzioni per lo sblocco"
        resend_confirmation_instructions: "Invia di nuovo le istruzioni per la conferma"
    unsupported_browser:
      headline: "Perfavore, notare che ActiveAdmin non supporta più Internet Explorer 8 o inferiore"
      recommendation: "Ti raccomandiamo di aggiornare alla versione più recente di <a href=\"http://windows.microsoft.com/ie\">Internet Explorer</a>, <a href=\"https://chrome.google.com/\">Google Chrome</a>, o <a href=\"https://mozilla.org/firefox/\">Firefox</a>."
      turn_off_compatibility_view: "Se stai utilizzando Internet Explorer 9 o successivo, assicurati di <a href=\"http://windows.microsoft.com/en-US/windows7/webpages-look-incorrect-in-Internet-Explorer\">disabilitare la \"Modalità Compatibilità\"</a>."
    access_denied:
      message: "Non hai le autorizzazioni necessarie per eseguire questa azione."
    index_list:
      table: "Tabella"
      block: "Lista"
      grid: "Griglia"
      blog: "Blog"
