class AddPhotoToNotis < ActiveRecord::Migration
  def up
    add_attachment :notis, :photo
  end

  def down
    remove_attachment :notis, :photo
  end
end
