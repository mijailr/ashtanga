class CreateOraris < ActiveRecord::Migration
  def change
    create_table :oraris do |t|
      t.integer :day
      t.time :from
      t.time :to
      t.references :location, index: true

      t.timestamps
    end
  end
end
