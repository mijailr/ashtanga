class CreateEventis < ActiveRecord::Migration
  def change
    create_table :eventis do |t|
      t.string :title
      t.text :description
      t.string :location
      t.datetime :date
      t.string :slug

      t.timestamps null: false
    end
    add_index :eventis, :slug, unique: true
  end
end
