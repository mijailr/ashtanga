class CreateNotis < ActiveRecord::Migration
  def change
    create_table :notis do |t|
      t.string :title
      t.text :content
      t.string :slug

      t.timestamps null: false
    end
    add_index :notis, :slug, unique: true
  end
end
