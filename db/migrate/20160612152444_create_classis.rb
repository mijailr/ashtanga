class CreateClassis < ActiveRecord::Migration
  def change
    create_table :classis do |t|
      t.string :site
      t.string :slug

      t.timestamps null: false
    end
    add_index :classis, :slug, unique: true
  end
end
