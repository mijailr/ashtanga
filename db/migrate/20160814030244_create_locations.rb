class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.string :address

      t.timestamps
    end
    Location.create([
      { name: "Urbino" },
      { name: "Fermignano"},
      { name: "Cagli"},
      { name: "Rimini"},
      { name: "Riccione", address: "Via Marsala 13"},
      { name: "Piobbico"}
      ])
  end
end
