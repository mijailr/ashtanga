class AddImageToEventi < ActiveRecord::Migration
  def up
    add_attachment :eventis, :photo
  end
  def down
    remove_attachment :eventis, :photo
  end
end
