from ruby:2.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /ashtanga
WORKDIR /ashtanga
ADD Gemfile /ashtanga/Gemfile
ADD Gemfile.lock /ashtanga/Gemfile.lock
RUN bundle install
ADD . /ashtanga
